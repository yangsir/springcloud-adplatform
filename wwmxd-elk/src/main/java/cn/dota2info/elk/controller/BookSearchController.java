package cn.dota2info.elk.controller;

import cn.dota2info.elk.Service.BookSearchService;
import cn.dota2info.elk.entity.BaseSearchParam;
import cn.dota2info.elk.entity.Book;
import cn.dota2info.elk.entity.MatchSearchParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.List;

@RequestMapping("booksearch")
@RestController
@Slf4j
public class BookSearchController {

    @Autowired
    private BookSearchService searchService;

    @RequestMapping("searchbyname")
    public Flux<Book> searchbyname(BaseSearchParam param) throws Exception {
        List<Book>  list=searchService.termQuery(param);
        return Flux.create(bookFluxSink -> {
            try {
                searchService.termQuery(param).forEach(bookFluxSink::next);
                bookFluxSink.complete();
            } catch (Exception e) {
                log.error("查询书籍出错:",e);
                bookFluxSink.error(new RuntimeException("查询出错"));
            }
        });
    }

    @RequestMapping("matchbyname")
    public Flux<Book> matchbysource(@RequestBody MatchSearchParam param){
        return Flux.create(bookFluxSink->{
            try {
                searchService.matchQuery(param).forEach(bookFluxSink::next);
                bookFluxSink.complete();
            } catch (Exception e) {
                log.error("查询书籍出错:",e);
                bookFluxSink.error(new RuntimeException("查询出错"));
            }
        });
    }
}
