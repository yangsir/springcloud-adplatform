package com.wwmxd.service.impl;

import com.wwmxd.entity.RoleMenu;
import com.wwmxd.dao.RoleMenuDao;
import com.wwmxd.service.RoleMenuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WWMXD
 * @since 2018-01-03 14:39:38
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuDao, RoleMenu> implements RoleMenuService  {
	
}
